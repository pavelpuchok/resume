---
pdf_options:
    format: A4
    margin: 10mm 20mm
stylesheet:
    - https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css
    - style.css
body_class: markdown-body
---

# Go/Node.js Developer
## Пучёк Павел

<p class="contacts">
Дата рождения: 29.04.1993</br>
E-mail: <a href="malito:pavel.puchok@gmail.com">pavel.puchok@gmail.com</a></br>
Телефон: +375 25 768 17 89</br>
LinkedIn: <a href="https://www.linkedin.com/in/pavelpuchok">www.linkedin.com/in/pavelpuchok</a></br>
</p>

### О себе

Я Javascript разработчик со стремлением развиваться в Go и практиках DevOps.
Я начинал в конце 2012 и прошел путь от независимого разработчика игр до _fullstack_ разработчика в _healthcare_ стартапе.

Моё главное хобби — это программирование. Также мне интересны способы повышения эффективности разработки.
Вот неполный список интересных для меня областей:
`CI/CD`, `Monitoring`, `IaC`, `Microservices`, `Kubernetes`, `XP`, `Linux`

### Навыки

- | -
- | -
Английский язык| Pre‐intermediate
Языки программирования | `Go`, `Typescript`, `Javascript`, `SQL`
Go| `godoc`, `goswagger`, `wire`, `gorrila/mux`, `gorrila/sessions`, `testify`
JS/TS | `Node.js`, `tape`, `Webpack`, `Express`, `React`, `Redux`
Ops, IaC | `Docker`, `Terraform`, `Packer`, `Vagrant`, `Ansible`, `Gitlab CI/CD`, `Jenkins`
Other | `OpenAPI 2.0`, `TDD`

### Курсы

#### DevOps практики и инструменты от OTUS (с февраля 2019 по сентябрь 2019)
На курсе я приобрёл хорошие знания о DevOps практиках и получил опыт применения необходимого инструментария.

**Технологии**: `Docker`, `Terraform`, `Packer`, `Vagrant`, `Ansible`, `Gitlab CI/CD`

### Опыт

#### ISsoft, Javascript Developer (с 2017 по октябрь 2019, 2 года)
Разработка мобильного приложения для healthcare стартапа США.
Самым интересным была интеграция с Amazon Alexa API, которая в тот момент находилась в закрытой бете, что требовало работы в тесном сотрудничестве с командой Amazon.

**Технологии**: `Node.js`, `Jenkins`, `AWS` (использование, не администрирование), `CouchDB`, `React`, `Redux`, `Cordova`, `Amazon Alexa`

#### Playtika, Техник-Программист (с 2013 по 2017, 4 года)
В мои обязанности входили интеграция и настройка ресурсов разрабатываемых игр (графика, анимации, звуки).
Главной особенностью было большое количество возможных автоматизации своей работы.
В данной роли я автоматизировал многие аспекты ежедневных задач с помощью написанных вспомогательных скриптов и инструментов.

**Технологии**: большая часть автоматизации производилась на чистом `Javascript` внутри семейства ПО Adobe: Animate (`JSFL`) и Photoshop (`JSX`). Скрипты и инструменты системного уровня: `Node.js` и `Python`
